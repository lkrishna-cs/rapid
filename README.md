# README #

This project demonstrates how to build an enrichment agent that uses a machine learning model built using [R](http://r-project.org).  It depends on the cogscale-python-sdk and Docker and uses the remote R server method of communicating between Python and R (see the Rserve package and the pyRserve python library).

/rapid : Contains the Python SDK modules for using R models using RPy2 (https://rpy2.github.io/doc/v2.9.x/html/index.html) package. This folder contains a docker file that can used to build an image using R 3.5.1 and Rpy2 along with their pre-requistes.

/rstudio : Contains a RStudio server docker image. This can be used to simulate connecting to RStudio server.

For local R Installations, try to use R 3.3 as it tend to work with all the RPy2 dependencies properly.

Necessary library packages for R includes:

a) Caret
b) DDPlot2
c) randomForest

These dependencies are already added to the docker image under the /rapid folder.
