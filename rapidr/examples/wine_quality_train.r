library(caret)
library(randomForest)

red <- read.csv('http://archive.ics.uci.edu/ml/machine-learning-databases/wine-quality/winequality-red.csv', header = TRUE, sep = ';')
white <- read.csv('http://archive.ics.uci.edu/ml/machine-learning-databases/wine-quality/winequality-white.csv', header = TRUE, sep = ';')

red <- read.csv('winequality-red.csv', header = TRUE, sep = ',')
white <- read.csv('winequality-white.csv', header = TRUE, sep = ',')

red[, 'color'] <- 'red'
white[, 'color'] <- 'white'
df <- rbind(red, white)
df$color <- as.factor(df$color)
good_ones <- df$quality >= 6
bad_ones <- df$quality < 6
df[good_ones, 'quality'] <- 'good'
df[bad_ones, 'quality'] <- 'bad'
df$quality <- as.factor(df$quality)

dummies <- dummyVars(quality ~ ., data = df)
df_dummied <- data.frame(predict(dummies, newdata = df))
df_dummied[, 'quality'] <- df$quality

trainIndices <- createDataPartition(df_dummied$quality, p = 0.7, list = FALSE)
train <- df_dummied[trainIndices, ]
test <- df_dummied[-trainIndices, ]

fitControl <- trainControl(method = 'cv', number = 5)
fitControl_rfe <- rfeControl(functions = rfFuncs, method = 'cv', number = 5)
fit_rfe <- rfe(quality ~., data = train, sizes = c(1:10), rfeControl = fitControl_rfe)
wine_quality_features <- predictors(fit_rfe)

wine_quality_model <- train(x = train[, wine_quality_features], y = train$quality, method = 'rf', trControl = fitControl, tuneGrid = expand.grid(.mtry = c(2:6)), n.tree = 1000)
#predict_rf <- predict(fit_rfe, newdata = test[, wine_quality_features])

save(wine_quality_features, file='wine_quality_features.rda')
save(wine_quality_model, file='wine_quality_model.rds')

rapid_predict <- function(model,x){
  y = predict(model, newdata=x)
  y_prob = predict(model, newdata=x,type="prob")
  list(y,y_prob)
}


x = test[1,]
r_predict = rapid_predict(wine_quality_model,x)
