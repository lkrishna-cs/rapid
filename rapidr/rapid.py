import os
import logging
import yaml
import argparse
from flask import Flask, jsonify
import flask_restful
from flask_restful_swagger import swagger
from flask_injector import FlaskInjector
from common.exceptions import ApiException

__version__ = "0.1"
__description__ = "Rapid - R API Server"

log = logging.getLogger()
formatter = logging.Formatter('%(asctime)s:%(module)s:%(threadName)s:%(message)s')
handler = logging.StreamHandler()
handler.setFormatter(formatter)
log.addHandler(handler)
log.setLevel(logging.INFO)

app = Flask(__name__)
api = swagger.docs(flask_restful.Api(app), apiVersion=__version__)

@app.errorhandler(ApiException)
def handle_api_exception(error):
    response = jsonify(error.to_dict())
    response.status_code = error.code
    return response


class Hello(flask_restful.Resource):

    def get(self):
        return {'name': __description__, 'version': __version__}

api.add_resource(Hello, '/')


def register_api():
    from api import register_api
    register_api(api, '/api/v1')

register_api()

config = {}

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__description__, add_help=True)
    parser.add_argument('--version', action='version', version='Rapid v%s' % __version__)
    parser.add_argument('--debug', action='store_true', dest='rapid_debug',
                        help='start Rapid in debug mode', default=False)
    parser.add_argument('-c', action='store', dest='conf_file',
                        help='path to the Rapid configuration file (rapid.yml)', default='rapid.yml')
    parser.add_argument('-p', action='store', dest='port',
                        help='port for the Rapid server to use', default=5555)

    args = parser.parse_args()

    rapid_debug = bool(os.getenv('RAPID_DEBUG', args.rapid_debug))
    if rapid_debug:
        print("Rapid starting in DEBUG mode")

    rapid_conf = os.path.abspath(args.conf_file)
    if os.path.isfile(args.conf_file):
        log.info('Loading Rapid configuration from %s' % rapid_conf)
        with open('rapid.yml') as f:
            config = yaml.load(f)
    else:
        log.error('Rapid configuration file not found %s' % rapid_conf)

    # Configure R models and inject model registry into APIs
    from api.models import init_models, inject_models
    init_models(config)
    FlaskInjector(app=app, modules=[inject_models])

    app.run(host='0.0.0.0', port=int(os.getenv('VCAP_APP_PORT', args.port)),
            debug=rapid_debug, use_reloader=rapid_debug)