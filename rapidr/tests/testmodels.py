import yaml
import json
from api.models import init_models,model_registry, _to_pandas_dataframe

def test_predict():
    with open('../rapid.yml') as f:
        config = yaml.load(f)
    init_models(config)

    model = model_registry.get_model('wine_quality')

    predict_req = json.load(open('../examples/predict_wine.json'))
    x = predict_req['x']
    x_labels = predict_req['x_labels']

    result = model.predict(x, x_labels)

    labels = result.predictions
    probs = result.probabilities

    for idx, label in enumerate(labels):
        label_col = getattr(probs, label)
        print (label, label_col[0])


def test_convert_to_df():
    predict_req = json.load(open('../examples/predict_wine.json'))
    x = predict_req['x']
    x_labels = predict_req['x_labels']

    df = _to_pandas_dataframe(x, x_labels)
    print(df)

def main():
    print ("Calling predict")
    test_predict()
    print("Called predict")

if __name__ == "__main__":
    main()
