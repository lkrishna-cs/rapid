

class ApiException(Exception):
    code = 400

    def __init__(self, message=None, code=None, data=None):
        Exception.__init__(self)
        self.message = message
        if code is not None:
            self.code = code
        self.data = data

    def to_dict(self):
        rv = dict(self.data or ())
        rv['message'] = self.message
        return rv