from collections import OrderedDict
from injector import singleton, Key, InstanceProvider
import rpy2.robjects as robjects
from rpy2.robjects import pandas2ri
from pandas import DataFrame


def _to_pandas_dataframe(x, x_labels):
    x_df = OrderedDict()
    for idx, val in enumerate(x):
        x_df[x_labels[idx]] = [val]

    return DataFrame(x_df)


class PredictResult(object):

    def __init__(self, predictions, probabilities):
        self.predictions = predictions
        self.probabilities = probabilities


class Model(object):

    def __init__(self, api_path, model_var, model_file):
        self.api_path = api_path
        self.model_var = model_var
        self.model_file = model_file

        robjects.r('''
            rapid_predict <- function(model, x) {
                y = predict(model, newdata=x)
                y_prob = predict(model, newdata=x, type="prob")
                list(y, y_prob)
            }
            ''')

    def predict(self, x, x_labels):
        pandas2ri.activate()
        x_df = _to_pandas_dataframe(x, x_labels)
        r_df = pandas2ri.py2ri(x_df)

        r_predict = robjects.r['rapid_predict']
        robjects.r['load'](self.model_file)
        model = robjects.r[self.model_var]

        y = r_predict(model, r_df)

        return PredictResult(list(y[0].levels), pandas2ri.ri2py(y[1]))


class ModelRegistry(object):

    def __init__(self):
        self.models = {}

    def register_model(self, name, model):
        self.models[name] = model

    def get_model(self, name):
        return self.models.get(name, None)

model_registry = ModelRegistry()
Models = Key('model_registry')


def init_models(config):
    config_models = config.get('models', {})

    for model_name in config_models:
        model_config = config_models[model_name]
        api_path = model_config['api_path']
        model_var = model_config['trained_model_var']
        model_file = model_config['trained_model_path']

        model_registry.register_model(model_name, Model(api_path, model_var, model_file))


def inject_models(binder):
    binder.bind(
        Models,
        to=InstanceProvider(model_registry),
        scope=singleton
    )
