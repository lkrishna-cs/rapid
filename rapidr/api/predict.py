from flask import request
from flask_restful import Resource
from injector import inject
from api.models import Models
from common.exceptions import ApiException


class Predict(Resource):


    #@inject(models=Models)
    def __init__(self, models):
        self.models = models

    def post(self, model_name):
        model = self.models.get_model(model_name)
        if model:
            predict_request = request.get_json(force=True)
            x = predict_request.get('x')
            x_labels = predict_request.get('x_labels')
            if x and x_labels:
                result = model.predict(x, x_labels)
                labels = result.predictions

                predictions = []
                for idx, label in enumerate(labels):
                    label_col = getattr(result.probabilities, label)
                    prob = label_col[idx]
                    predictions.append({'label': label, 'probability': prob})

                return {'predictions': predictions}
            else:
                raise ApiException(message="Predict request was missing required attributes", code=400)
        else:
            raise ApiException(message="Model with name %s not found" % model_name, code=404)