import rpy2.robjects as robjects
from flask_restful import Resource


class Pi(Resource):

    def get(self):
        pi = robjects.r['pi']
        return {'pi': pi[0]}