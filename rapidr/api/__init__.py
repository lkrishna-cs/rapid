from api.pi import Pi
from api.predict import Predict

def register_api(api, base_uri):
    print("Registering APIs")
    api.add_resource(Pi, "%s/%s" % (base_uri, "healthcheck/pi"))
    api.add_resource(Predict, "%s/%s" % (base_uri, "predict/<model_name>"))